import pyaudio
import numpy as np
import sys

if (len(sys.argv)!=2):
	exit(0)


colorid=['\033[90m█','\033[90m█','\033[95m█','\033[94m█','\033[96m█','\033[92m█','\033[93m█','\033[97m█']


CHUNK = int(sys.argv[1]) #Número de puntos a leer cada vez
RATE = 160000 #Resolución de tiempo del dispositivo de grabación (Hz)

p=pyaudio.PyAudio() #Inicia PyAudio
stream=p.open(format=pyaudio.paInt16,channels=1,rate=RATE,input=True, frames_per_buffer=CHUNK) #Escucha del dispositivo de entrada predeterminado

try:
	while (True): #Looooop
		data = np.fromstring(stream.read(CHUNK),dtype=np.int16)	#Obtiene los datos de audio
		fft = abs(np.fft.fft(data).real)
		fft = fft[:int(len(fft)/32)] #Recorta los valores no deseados

		#String donde irán los datos conseguidos
		cadena=""

		#Añade los datos a cadena como cuadrados de colores
		for val in fft: 
			for i in range(7,0,-1):
				expo=14**i
				if (val > expo):
					cadena+=colorid[i-1]
					break

		#Imprime los datos
		print(cadena)

except:
	#Cierra el programa
	stream.stop_stream()
	stream.close()
	p.terminate()