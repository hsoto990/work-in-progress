import numpy as np
import cv2

COLOR_ROWS = 80
COLOR_COLS = 250

colorid=['  ','\033[90m██','\033[91m██','\033[95m██','\033[94m██','\033[96m██','\033[92m██','\033[93m██','\033[97m██']

capture = cv2.VideoCapture(0)
if not capture.isOpened():
	raise RuntimeError('Error opening VideoCapture.')

(grabbed, frame) = capture.read()
snapshot = np.zeros(frame.shape, dtype=np.uint8)

resolution=0.5
width=int(100*resolution)
height=int(70*resolution)

def perframe():
	#Obtiene la imagen
	snapshot = cv2.resize(frame.copy(), (width,height), interpolation = cv2.INTER_AREA) 
	cadena=""
	for iy in range(height-1):
		for ix in range(width-1):
			crgb=snapshot[iy, ix, [2,1,0]]
			# From stackoverflow.com/questions/1855884/determine-font-color-based-on-background-color
			luminance = ((0.299*crgb[0] + 0.587*crgb[1] + 0.114*crgb[2]) / 255)*1.6
			if (luminance > 1):
				caracter=colorid[8]
			else:
				for i in range (8,-1,-1):
					
					if (luminance > (i/9)):
						caracter=colorid[i]
						break
			cadena+=caracter
			#print(snapshot[iy, ix, [2,1,0]])
		cadena+="\n"
	print(cadena)
	print("")

try:
	while True:
		(grabbed, frame) = capture.read()
		cv2.waitKey(1)
		perframe()

except KeyboardInterrupt:
	capture.release()